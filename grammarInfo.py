import json

f = open("gramatica", "r")
lines = f.readlines()
f.close()

lines = map(lambda x: x.strip(), lines)
lines = filter(lambda x: len(x), lines)

info = {}

i = 0
for line in lines:

    rule_len = len(line.split()) - 2
    rule_name = line.split()[0]
    print(rule_len, rule_name, i, line)

    info[i] = {"name": rule_name, "len": rule_len, "rule": line}
    i += 1

f = open("grammarInfo.json", "w")
f.write(json.dumps(info, indent=4))
f.close()
print(lines)
