function getTable() {

  console.log("printa isso");

  const table = document.getElementById("lrTableView");
  const symbolsTh = table.querySelectorAll("thead > tr:nth-child(3) > th");
  const nActions = table.querySelector("thead > tr:nth-child(2) > th:nth-child(2)").getAttribute("colspan");
  const nGotos = table.querySelector("thead > tr:nth-child(2) > th:nth-child(3)").getAttribute("colspan");

  const symbols = [];
  for (let i = 0; i < symbolsTh.length; i++) {
    symbols.push(symbolsTh[i].innerText);
  }

  const actionSymbols = symbols.slice(0, nActions);
  const gotoSymbols = symbols.slice(nActions, nActions + nGotos);

  const lines = [];
  const tbodyTrs = table.querySelectorAll("tbody > tr");

  console.log("symbols are", symbols);

  finalJSON = {
    table: [],
    actionSymbols: actionSymbols,
    gotoSymbols: gotoSymbols
  };

  for (let i = 0; i < tbodyTrs.length; i++) {
    const cols = [];
    const tds = tbodyTrs[i].children;

    const tableLine = {};
    finalJSON.table.push(tableLine);
    
    for (let j = 1; j < tds.length; j++) {
      const symbol = symbols[j - 1];
      const value = tds[j].innerText;
      tableLine[symbol] = value.trim() || null;

      cols.push(tds[j].innerText);
    }
    lines.push(cols);
  }

  console.log(JSON.stringify(finalJSON));

  return finalJSON;
}
getTable();
