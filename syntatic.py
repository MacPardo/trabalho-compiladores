import lexic
import sys
import json
import copy

SLR = {}
TABLE = {}
ACTION = []
GOTO = []
GRAMMAR_INFO = {}

def setup():
    global SLR
    global TABLE
    global ACTION
    global GOTO
    global GRAMMAR_INFO

    f = open("slr.json", "r")
    content = f.read()
    f.close()
    SLR = json.loads(content)
    TABLE = SLR["table"]
    ACTION = SLR["actionSymbols"]
    GOTO = SLR["gotoSymbols"]

    f = open("grammarInfo.json", "r")
    content = f.read()
    f.close()
    GRAMMAR_INFO = json.loads(content)

def parse(symbol_table):
    tape = copy.deepcopy(symbol_table)
    tape.append({"state": -1, "value": "$", "line": -1, "token": "$"})

    stack = [0]

    while True:
        top = stack[-1]
        token_dict = tape[0]
        token_type = token_dict["token"]

        val = TABLE[top][token_type]
        if val == "acc":
            print("success!!!!")
            return True
        if val == None:
            print("Invalid token", tape[0])
            return False

        # Shift
        elif val[0] == 's':
            print()
            print(val)
            print("tape:", tape)
            print("stack:", stack)
            num = int(val[1:])
            stack.append(token_type)
            stack.append(num)
            tape.pop(0)

        # Reduce
        elif val[0] == 'r':


            numStr = val[1:]

            rule_info = GRAMMAR_INFO[numStr]

            stack = stack[:len(stack) - rule_info['len'] * 2]
            goto = TABLE[stack[-1]][rule_info['name']]

            stack.append(rule_info['name'])
            stack.append(int(goto))

            print()
            print(val)
            print("Rule:", rule_info["rule"])
            print("tape:", tape)
            print("stack:", stack)


        else:
            print("Something went wrong")
            return False
    
def main():
    lexic.make_transition()
    setup()

    if len(sys.argv) == 1:
        print("needs filename param")
        sys.exit()
    
    file_name = sys.argv[1]
    f = open(file_name, "r")
    content = f.read()
    f.close()
    symbol_table = lexic.get_complete_symbol_table(content)

    print(GRAMMAR_INFO)

    parse(symbol_table)

if __name__ == '__main__':
    main()
