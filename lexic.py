import main
import sys
import json

EPSILON = '%'
TERMINALS = []
TRANSITION_TABLE = []
FINAL_STATES = []
#SEPARATORS = " \t\n+-/=;()"
SEPARATORS = [">=", "==", " ", "\t", "\n", "+", "-", "/", "=", ";", "(", ")", "{", "}"]
IGNORED_CHARS = " \t\n"
STATE_TO_TOKEN = {}

def make_transition():
    arquivo = open('saidaAFD', 'r')
    lines = arquivo.readlines()
    arquivo.close()
    qtd_estados = int(lines[0])
    
    terminals = lines[1].strip().split(' ')

    global TERMINALS
    TERMINALS = terminals
    
    table_matrix = list(map(
        lambda x: list(map(int, x.strip().split(' '))), 
        lines[2:qtd_estados + 2]
    ))
    final_states = list(map(int, lines[-1].strip().split(' ')))

    def row_to_dict(row):
        dict = {}
        for state, term in zip(row, terminals):
            dict[term] = state
        return dict

    transition_table = list(map(row_to_dict, table_matrix))

    global TRANSITION_TABLE
    TRANSITION_TABLE = transition_table

    global FINAL_STATES
    FINAL_STATES = final_states

    global STATE_TO_TOKEN
    try:
        f = open("stateToToken.json", "r")
        STATE_TO_TOKEN = json.loads(f.read())
        f.close()
    except:
        STATE_TO_TOKEN = None
        


def get_word(string, delimeters):
    delim = get_biggest_delimeter(string, delimeters)
    if len(delim):
        return delim, string[len(delim):]
    for i in range(len(string)):
        if len(get_biggest_delimeter(string[i:], delimeters)):
            return string[:i], string[i:]
    return string, ""

def get_words(string, delimeters):
    words = []
    while string:
        word, string = get_word(string, delimeters)
        words.append(word)
    return words

def get_biggest_delimeter(string, delimeters):
    # esta funcao assume que delimeters esta ordenado em
    # ordem decrescente de comprimento

    for delim in delimeters:
        if string.startswith(delim):
            return delim
    return ""

def filter_ignored_chars(string, ignored_chars):
    return "".join([s for s in string if s not in ignored_chars])

def filter_ignored_chars_from_words(words, ignored_chars):
    words = [filter_ignored_chars(word, ignored_chars) for word in words]
    return [word for word in words if len(word) > 0]

def split_and_filter(string, delimeters, ignored_chars):
    word_list = get_words(string, delimeters)
    return filter_ignored_chars_from_words(word_list, ignored_chars)

def get_symbol_table(words, line):
    symbol_table = []
    for word in words:
        state = 1
        for char in word:
            if char not in TERMINALS:
                print("Invalid character '" + char + "' in line " + str(line))
                sys.exit()
            state = TRANSITION_TABLE[state][char]
        if state == 0:
            print("ERROR: invalid token: " + word + " at line " + str(line))
            print("symbol table so far is")
            print(symbol_table)
            sys.exit()

        global STATE_TO_TOKEN
        symbol_dict = {"state":state, "value":word, "line":line}

        if STATE_TO_TOKEN:
            print("the token", symbol_dict["value"], "is being recognized by", state)
            symbol_dict["token"] = STATE_TO_TOKEN[str(state)]

        symbol_table.append(symbol_dict)
    return symbol_table

def get_complete_symbol_table(string):
    global SEPARATORS
    global IGNORED_CHARS

    lines = string.split("\n")
    symbol_table = []
    for line, i in zip(lines, range(len(lines))):
        words = split_and_filter(line, SEPARATORS, IGNORED_CHARS)
        symbol_table += get_symbol_table(words, i)
    return symbol_table
        

def main():
    make_transition()
    print(sys.argv)

    if len(sys.argv) == 1:
        print("forneça o arquivo de entrada")
        sys.exit()

    input_file_name = sys.argv[1]

    input_file = open(input_file_name, 'r')
    input_string = input_file.read()
    print(input_string)
    input_file.close()

    symbol_table = get_complete_symbol_table(input_string)
    table_json = json.dumps(symbol_table, indent=4)
    f = open("symbolTable.json", "w")
    f.write(table_json)
    f.close()

    if (len(sys.argv) >= 3 and sys.argv[2] == "redefine"):
        stateToToken = {}
        tokenToState = {}

        for sym in symbol_table:
            stateToToken[sym["state"]] = sym["value"]
            tokenToState[sym["value"]] = sym["state"]

        print(stateToToken)
        print("====")
        print(tokenToState)

        f = open("stateToToken.json", "w")
        f.write(json.dumps(stateToToken, indent=4))
        f.close()

        f = open("tokenToState.json", "w")
        f.write(json.dumps(tokenToState, indent=4))
        f.close()



if __name__ == '__main__':
    main()
